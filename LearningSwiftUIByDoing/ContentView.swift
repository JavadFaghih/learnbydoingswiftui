//
//  ContentView.swift
//  LearningSwiftUIByDoing
//
//  Created by javad faghih on 1/26/1400 AP.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hi, SwiftUI")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
