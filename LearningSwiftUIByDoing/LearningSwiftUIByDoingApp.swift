//
//  LearningSwiftUIByDoingApp.swift
//  LearningSwiftUIByDoing
//
//  Created by javad faghih on 1/26/1400 AP.
//

import SwiftUI

@main
struct LearningSwiftUIByDoingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

struct LearningSwiftUIByDoingApp_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
